<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="webjars/bootstrap/5.0.1/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
	<form:form action="registration" modelAttribute="customer" method="post">
	<div>
		ID: <form:input path="id"/>
	</div>
	<div>
		FIRST NAME: <form:input path="firstName"/>
		<form:errors style="colors:red" path="firstName"></form:errors>
	</div>
	<div>
		LAST NAME: <form:input path="lastName"/>
	</div>
	<div>
		GENDER: <form:input path="gender"/>
	</div>
	<div>
		PHONE NUMBER: <form:input path="phoneNumber"/>
	</div>
	<div>
		<input type="submit" value="register"/>
	</div>
	</form:form>
	<script src="webjars/jquery/3.6.0/jquery.min.js"></script>
    <script src="webjars/bootstrap/5.0.1/js/bootstrap.min.js"></script>
</body>
</html>