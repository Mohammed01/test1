package com.cts.proj.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cts.proj.model.Customer;
@Service
public class CustomerDAO {
	private static List<Customer> customers;
	public CustomerDAO() {
		customers =new ArrayList<Customer>();
		Customer c1=new Customer(101,"aaa","surname","M","1234567890");
		Customer c2=new Customer(102,"bbb","surname1","f","2234567890");
		customers.add(c1);
		customers.add(c2);
	}
	public boolean addCustomer(Customer c) {
		return customers.add(c);
	}
	 public Customer findCustomer(int id) {
		 return customers.stream()
				 .filter((c)->{return c.getId()==id;})
				 .findFirst()
				 .orElse(null);
	 }
	 public List<Customer> allCustomer(){
		 return customers;
	 }
}
