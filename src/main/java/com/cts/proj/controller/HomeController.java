package com.cts.proj.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cts.proj.dao.CustomerDAO;
import com.cts.proj.model.Customer;

@Controller
public class HomeController {
	private static final Logger LOGGER=Logger.getLogger("HomeController");
	
	@Autowired
	CustomerDAO service;
	
	@RequestMapping("/")
	public String greet() {
		return "login";
	}
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String loginUser(@RequestParam("username") int username,@RequestParam("password") String userpass, Model model) {
		Customer c=service.findCustomer(username);
		model.addAttribute("user", c);
		return "userhome";
	}
	
	@RequestMapping("/registration")
	public String getRegForm(Model model) {
		Customer c=new Customer();
		model.addAttribute("customer", c);
		return "regform";
	}
	@RequestMapping(value="/registration",method=RequestMethod.POST)
	public String processReg(@ModelAttribute("customer") Customer cust) {
		service.addCustomer(cust);
		return "login";
	}
	
	@RequestMapping("/allcustomers")
	public String allCustomers(Model model) {
		List<Customer> customers=service.allCustomer();
		model.addAttribute("customers", customers);
		return "customerlist";
	}
}
